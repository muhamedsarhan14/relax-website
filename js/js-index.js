const maditaionApp = () => {
    const song = document.querySelector(".song");
    const play = document.querySelector(".play");
    const outlineMoving = document.querySelector(".moving-outline circle");
    const video =document.querySelector(".vid-container video");
    
    const sound  = document.querySelectorAll(".app-music button");
    const appTime = document.querySelectorAll(".app-time button")
    const timeDisplay = document.querySelector(".time-display");
    
    const outlineLength = outlineMoving.getTotalLength();

    let fakeDuration = 600;
    
    outlineMoving.style.strokeDasharray = outlineLength;
    outlineMoving.style.strokeDashoffset = outlineLength;
    
    sound.forEach(sound => {
        sound.addEventListener("click" , function(){
            song.src = this.getAttribute("data-sound");
            video.src = this.getAttribute("data-video");
            wellDone(song);
        } )
    })
    
    
    
    
    play.addEventListener("click", () => {
       
        wellDone(song);
    });
    
    appTime.forEach(option => {
        option.addEventListener("click", function(){
            fakeDuration = this.getAttribute("data-time");
            timeDisplay.textContent = `${Math.floor(fakeDuration / 60)}:${fakeDuration % 60}0`;
        })
    })
    
    const wellDone = song => {
        if(song.paused){
            song.play();
            video.play();
            play.src = './svg/pause.svg';
        }else
            {
                song.pause();
                video.pause();
                play.src = './svg/play.svg';
            }
    }
    song.ontimeupdate = () => {
        let currentTime = song.currentTime;
        console.log(currentTime);
        let elapsed = fakeDuration - currentTime ;
        let minutes = Math.floor(elapsed / 60);
        let seconds = Math.floor(elapsed % 60);
        
        let progress = outlineLength - ( currentTime / fakeDuration ) * outlineLength;
        outlineMoving.style.strokeDashoffset = progress;
        timeDisplay.textContent = `${minutes}:${seconds}`;
        if(currentTime >= fakeDuration){
            song.pause();
            video.pause();
            song.currentTime = 0;
            play.src = './svg/play.svg'
        }
    }
    
    
}
maditaionApp();